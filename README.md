Ants Minimal
============

A straightforward terminal-like human interface to Ants (http://ants.aichallenge.org), along with the minimum  infrastructure to play a game.

It is intended as a learning tool for new Ants programmers.  A very simple gui is provided, written in Python with Tkinter, which reads the game info from the game engine and provides a text entry box to write moves back.

To play a game, execute

    ./play_one_game.sh

and the gui should come up, having been launched as a bot by the game engine.  To complete a turn, type 'go' into the text box.  You will need to do this after the initial game parameters are displayed, and the game engine will then send the turn info for the first turn.  Make a move by entering eg

    o 2 2 n

to move the ant at 2, 2 north. Don't forget to enter 'go' when you have finished your turn.  

You can replay the game afterwards using

    ./replay

Greg, 19 May 2015
gregokeefe at the domain netspace.net.au
