#!/usr/bin/env sh
tools/playgame.py \
    --turns 20  \
    --loadtime=100000 \
    --turntime=300000 \
    --player_seed 42 \
    --end_wait=0.25  \
    --food_start 100 100 \
    --food_rate 0 0 \
    --food_turn 1 1 \
    --verbose \
    --capture_errors \
    --log_error  \
    --log_stderr \
    --log_dir game_logs  \
    --map_file little.map \
    "$@" \
    "python manualBot.py"  \
    "python tools/sample_bots/python/HunterBot.py"


# tools/maps/maze/maze_04p_01.map

# "lua5.1 MyBot.lua"
# "../AntsC/MyBot" 
# "python tools/sample_bots/python/HunterBot.py" 
# "python tools/sample_bots/python/GreedyBot.py"

# --nolaunch
