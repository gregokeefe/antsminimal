#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
A minimal tkinter interface to allow a human to play ants.
Displays the output from the game engine.
Accepts moves one line at a time from the user.

Greg, 20 May 2015
derived from various online tutorials.
"""

from Tkinter import *

debug = True
input_poll_ms = 100
input_lines = 20
output_cols = 40

class AntsInterface(Frame):
  
    def __init__(self, parent):
        Frame.__init__(self, parent, background="white")   
        self.parent = parent
        self.initUI()
        self.readTurn()

    def initUI(self):
        self.parent.title("Ants Interface")
        self.pack(fill=BOTH, expand=1)

        self.t = Text(self.parent, height=input_lines, width=output_cols)
        self.t.pack(side=TOP)

        self.e = Entry(self.parent, width=output_cols)
        self.e.bind('<Return>', self.move)
        self.e.pack(side=BOTTOM)
        self.e.focus_set()
                       
    def move(self, text):
        t = self.e.get()
        sys.stdout.write(t)
        sys.stdout.write('\n')
        sys.stdout.flush()
        self.e.delete(0,END)
        if t == "go":
            self.readTurn()

    def readTurn(self):
        self.t.delete("0.0",END)
        self.e.insert(0, "reading ...")
        while True:
            input = sys.stdin.readline().rstrip('\r\n')
            self.t.insert(END, input)
            self.t.insert(END, "\n")
            if input == "go" or input == "ready":
                self.e.delete(0,END)
                break
            
def main():
  
    root = Tk()
    root.geometry("250x320+300+100")
    app = AntsInterface(root)
    root.mainloop()

if __name__ == '__main__':
    main()  
